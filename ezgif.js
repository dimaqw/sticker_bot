const https = require('https');
const path = require('path');
const { transformTypes } = require('./constant');

function transform(file, transformType) {
  file = path.basename(file);
  let postData;

  switch (transformType) {
  case transformTypes.flipHorizontal:
    postData = `file=${file}&flop=on`;
    break;
  case transformTypes.monochrome:
    postData = `file=${file}&monochrome=on`;
    break;
  case transformTypes.gotham:
    postData = `file=${file}&gotham=on`;
    break;
  case transformTypes.vignette:
    postData = `file=${file}&vignette=on&vignette-blur=102&vignette-bg=%23000000`;
    break;
  default:
    postData = `file=${file}&flop=on`;
    break;
  }

  const options = {
    hostname: 'ezgif.com',
    port: 443,
    path: `/effects/${file}`,
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': Buffer.byteLength(postData)
    }
  };

  return new Promise((resolve, reject) => {
    const req = https.request(options, (res) => {
      console.log(`STATUS: ${res.statusCode}`);
      console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
      res.setEncoding('utf8');
      let body = [];

      res.on('data', (chunk) => {
        body.push(chunk);
      }).on('end', () => {
        body = [].concat(body).toString();
        resolve(body);
      });
    });

    req.on('error', (err) => {
      console.error(`problem with request: ${err.message}`);
      reject(err);
    });

    req.write(postData);
    req.end();
  });
}

function putToEzgif(url) {
  return new Promise((resolve, reject) => {
    https.get(`https://ezgif.com//webp-to-png?url=${url}`, (resp) => {
      //just empty handler, to be sure that data listener is registered
      resp.on('data', () => {});
      resp.on('end', () => {
        resolve(resp.headers.location);
      });
    }).on("error", (err) => {
      console.log("Error: " + err.message);
      reject(err);
    });
  });
}

module.exports = {
  putToEzgif,
  transform
};