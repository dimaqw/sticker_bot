const cheerio = require('cheerio');

function extractLink(body) {
  return new Promise((resolve, reject) => {
    try {
      let $ = cheerio.load(body);
      const link = 'https:' + $(".outfile img").attr('src');
      console.log(link);
      resolve(link);
    }catch (e) {
      reject(e);
    }
  });
}

module.exports = {
  extractLink
};
