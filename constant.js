module.exports = {
  transformTypes: {
    flipHorizontal: 'FLIP_HORIZONTAL',
    monochrome: 'MONOCHROME',
    gotham: 'GOTHAM',
    vignette: 'VIGNETTE',
  }
};