const TelegramBot = require('node-telegram-bot-api');

const { putToEzgif, transform } = require('./ezgif');
const { extractLink } = require('./cheerio');
const { transformTypes } = require('./constant');

const token = '<YOUR_TOKEN>';
const telegramUrl = 'https://api.telegram.org';
const filePrefix = `${telegramUrl}/file/bot${token}/`;

const bot = new TelegramBot(token, { polling: true });
const Users = {};

const buttons = {
  [transformTypes.flipHorizontal]: 'Flip ↔️ (horizontal)',
  [transformTypes.monochrome]: 'Monochrome 🔳',
  [transformTypes.gotham]: 'Gotham 😈',
  [transformTypes.vignette]: 'Vignette 🌅',
  close: 'Close ❎',
};


bot.onText(/\/test/, (msg) => {
  console.log(msg);
});

bot.on('sticker', (msg) => {
  console.log(msg);
  Users[msg.chat.id] = msg.sticker.file_id;

  bot.sendMessage(msg.chat.id, "Choose transformation type", {
    reply_markup: {
      keyboard: [[buttons[transformTypes.flipHorizontal], buttons[transformTypes.monochrome]],
        [buttons[transformTypes.gotham], buttons[transformTypes.vignette]],
        [buttons.close]],
      resize_keyboard: true
    }
  }).then((data) => {
    console.log(data);
  });

});


bot.on('text', (msg) => {
  console.log(msg);
  const chatId = msg.chat.id;
  // if (!Users[msg.chat.id]) {
  if (!msg.sticker && !Users[msg.chat.id]) {
    bot.sendMessage(chatId, 'Please send me a sticker');
  }else{
    switch (msg.text){
    case buttons[transformTypes.flipHorizontal]:
      stickerProcessing(msg, transformTypes.flipHorizontal);
      break;
    case buttons[transformTypes.vignette]:
      stickerProcessing(msg, transformTypes.vignette);
      break;
    case buttons[transformTypes.monochrome]:
      stickerProcessing(msg, transformTypes.monochrome);
      break;
    case buttons[transformTypes.gotham]:
      stickerProcessing(msg, transformTypes.gotham);
      break;
    case buttons.close:
      Users[msg.chat.id] = null;
      bot.sendMessage(msg.chat.id, 'Do you like it? Send another sticker.', { reply_markup: { remove_keyboard: true } });
      break;
    default:
      stickerProcessing(msg, transformTypes.flipHorizontal);
      break;
    }
  }

});

function stickerProcessing(msg, transformType) {
  bot.getFile(Users[msg.chat.id])
    .then((telegramFile) => {
      console.log(telegramFile, 'telegramFile');
      const fileLink = filePrefix + telegramFile.file_path;
      console.log(fileLink, 'fileLink');
      return putToEzgif(fileLink);
    }).then((ezgifUrl) => {
      console.log(ezgifUrl, 'ezgifUrl');
      return transform(ezgifUrl, transformType);
    }).then((body) => {
      // console.log(body);
      return extractLink(body);
    }).then((link) => {
      return bot.sendSticker(msg.chat.id, link, { name: 'test' });
    });
}